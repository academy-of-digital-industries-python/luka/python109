# Python

### Resources

* [Books](https://drive.google.com/drive/folders/1itCkAhABmjKIydPt0_Y-qJb4QxvPsXXQ?usp=share_link)
* [Computerphile (CS topics)](https://www.youtube.com/@Computerphile)
* [Tech With Tim (Python)](https://www.youtube.com/@TechWithTim)
* [Final Project](https://github.com/tavkhelidzeluka/python109FinalProject.git)
### Homeworks
---

Create your github repository, where you will store all your homework code

[how to create your github repo?](#how-to-upload-homework)

* [Homework 1](https://docs.google.com/document/d/10UlqlipSXl6JRX1FMyz2Ys6KZruW2XW6/edit?usp=share_link&ouid=104943108488678283790&rtpof=true&sd=true)
* [Homework 2](https://docs.google.com/document/d/1uaZnETojghiVT0_yMwrHyBKpIYFzpYEx/edit?usp=share_link&ouid=104943108488678283790&rtpof=true&sd=true)
* [Homework 3](https://docs.google.com/document/d/1Cw1cw5ID9Bo68LowDiZtKo3SY_gfGuBt/edit?usp=share_link&ouid=104943108488678283790&rtpof=true&sd=true)
* [Homework 4](https://docs.google.com/document/d/1AnuUaQAD0Lii7TFJihIRf5YChaZgxJNq/edit?usp=sharing&ouid=104943108488678283790&rtpof=true&sd=true)
* [Homework 5](https://docs.google.com/document/d/1hAOiFXEonZ-C4ALsDtYiAKC3vDhab67q/edit?usp=share_link&ouid=104943108488678283790&rtpof=true&sd=true)

### Challenges
* [Challenge 1](https://docs.google.com/document/d/1VbbSmeZN50Dd9BXJnaMeTNHYnH4ap-Qb/edit?usp=share_link&ouid=104943108488678283790&rtpof=true&sd=true)


### How to upload homework
---

1. Log in to [github](https://github.com/).
2. Press green button "new" ![](.assets/new%20repo.png)
3. Choose name for your repository and make it public
4. Clone repository to your local machine
5. Write some code
6. Push some code 
7. Go to 5th point! (repeat)
