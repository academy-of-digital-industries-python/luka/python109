'''
Data Structure
Dictionary - ლექსიკონი
dict - in python
'''
student = {
    'name': 'Nick', 
    'age': 21,
    'pet': 'Cat',
}

# students = []

# access
print(student, type(student))
print(student['name'])
print(student['age'])
print(student['pet'])

# print(students, type(students))

# a = list(range(100_000_000))
# print(sys.getsizeof(a))

# change
student['name'] = 'Mary'

# new keys
student['vehicle'] = 'Tesla'

# removing keys
student.pop('pet')

a = None

# get method
print(student.get('father'))