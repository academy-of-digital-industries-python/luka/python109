"""
Data Structure - Tuple
immutable type
"""
names = ("Josh", 'john', 'Nick', 'Dave')

# Iterate trough
# for name in names:
#     print(name)

print(names[0])
print(names.index('Nick'))
print(names[2][1])

# names[0] = 5