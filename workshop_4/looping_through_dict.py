lecturer = {
    'name': 'Luka',
    'age': 22,
    'room': 22
}

print('\nOnly Keys')
for key in lecturer.keys():
    # print(key, lecturer[key])
    print(key)

# for key in lecturer:
#     print(key, lecturer[key])

print('\nValues only')
for value in lecturer.values():
    print(value)

print('\nItems')
for key, value in lecturer.items():
    print(key, value)
