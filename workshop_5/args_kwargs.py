"""
create a function that takes at least 2 arguments and returns their sum
"""
from typing import Any

def shekribe(a: float, b: float, *args: tuple[float]) -> float:
    s: float = a + b
    for number in args:
        s += number
    return s

result = shekribe(1, 2, 3, 4, 5, 6)
print(result)


def func_with_kwargs(**kwargs: dict[str, Any]) -> None:
    print(kwargs)


k = dict(name='Luka', number_2=7, car='Tesla')
func_with_kwargs(**k)
# print(*[1, 2, 3, 4])
