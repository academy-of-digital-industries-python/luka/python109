
# range function

# list comprehension
odd_numbers = [
    number
    for number in range(1, 10, 2)
]

# list comprehension with conditional
# odd_numbers = [
#     i
#     for i in range(10)
#     if i % 2 == 1
# ]

# odd_numbers = []
# for i in range(10):
#     if i % 2 == 1:
#         odd_numbers.append(i)

# for i in range(1, 10, 2):
#     odd_numbers.append(i)


"""
1. range(stop) -> default it start from 0, step = 1
2. range(start, stop) 
3. range(start, stop, step)
"""

# for number in odd_numbers:
#     print(number)

# print(odd_numbers[0])

# for i in range(len(odd_numbers)):
#     print(i, odd_numbers[i])
    

for i, number in enumerate(odd_numbers):
    print(i, number)

