class Transport:
    """Parent"""
    def __init__(self, brand: str, model: str, year: int) -> None:
        self.brand = brand
        self.model = model
        self.year = year
    
    def get_descriptive_name(self) -> str:
        return f'{self.brand} - {self.model} - {self.year}'


class Airplane(Transport):
    pass


class Bus(Transport):
    """Child"""
    def __init__(self, color: str, brand: str, model: str, year: int) -> None:
        super().__init__(brand, model, year)  # calls parent class's init method
        self.color = color

    def get_descriptive_name(self) -> str:
        # method overriding
        return f'<Bus> {self.color} {super().get_descriptive_name()}'


class FlyingBus(Bus, Airplane):
    def get_descriptive_name(self) -> str:
        return f'<FlyingBus> {super().get_descriptive_name()}'


class Sedan(Transport):
    """Child"""
    pass


transports: list[Transport] = [
    Bus("Blue", "Man", "c5", 2022),
    Sedan("Volvo", "c5", 2022),
    FlyingBus("Blue", "Man", "c5", 2022)
]
for transport in transports:
    print(transport.get_descriptive_name())
