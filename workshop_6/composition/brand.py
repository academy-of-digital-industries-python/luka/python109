class Brand:
    def __init__(self, name: str, founded: int) -> None:
        self.name = name
        self.founded = founded
    
    def get_descriptive_name(self) -> str:
        return f'{self.name} - {self.founded}'

