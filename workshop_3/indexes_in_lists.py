odd_numbers = [1, 7, 23, 27]

"""
find last element in the list dynamically
"""
print(len(odd_numbers))
# hard coding
# print(odd_numbers[4])

# print(odd_numbers[len(odd_numbers) - 1])

# negative index
print(odd_numbers[-1])
