'''
Docstring - Multiline comment

Data Types
'''

"""
* Integer - მთელი რიცხვები Z
int - in python
(-INF, -2, -1, 0, 1, 2, +INF)
"""
age = 23

print(type(age))

print(age + 2)
print(age - 2)
print(age / 2)
print(age * 2)

print(age // 2)
print(age ** 2)
# modulus - ნაშთის ოპერატორი
print(age % 2)

"""
Check this out!

print(0.1 + 0.2)  # 0.3
print(0.3)
"""


"""
String - ტექსტი
str - in python
"""

name = 'Gela'
name2 = "Jane"
name3 = """Jake"""
name4 = '''Mary'''


print(name)
print(name2)
print(name3)
print(name4)

# concatenation (Slow operation!)
print(name + ' ' + name2)
# fstrings
print(f'{name} and {name2}')
print(f'Original: {name2}; in lower case: {name2.upper()}')


"""
Variable naming
Allowed:
name1
full_name
first_name
username
home_address

Reccomendation:
use meaningful long names (don't use single characters, a, b, n)


Restricted:
1231231 <-> Error
1name <-> Error
%$@sfwe/ <-> Error

Reserved:
int
str
print
"""

name = 5
print(name)

"""
CONSTANTS variable
SCREAMING_SNAKE_CASE
"""
FIRST_NAME = f"Jane loves {name4}"

print(FIRST_NAME)
"""
FIRST_NAME = 5  # don't do this!!!
print(FIRST_NAME)
"""


"""
Fractions - წილადი რიცხვები Q
float - in python
(-INF, -1, -0.9, -0.98, 0, 0.6, 0.9, 1, +INF)
"""


pi = 3.14

print(pi / 2)


"""
3 - 7
2 - 5
1 - 3
0 - 1
-1 - 2
-2 - 4
-3 - 6
-4 - 8
"""
