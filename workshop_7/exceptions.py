""" Error handling """

class BadUserError(Exception):
    pass
    
# try-except
try:
    print("Hello")
    6 / 0
except TypeError:
    print("You tried to add str with int")
except ZeroDivisionError:
    print("You tried to divide number by 0")
except Exception as e:
    print('Unknown error occurred!')
    print(e, type(e))
# ZeroDivisionError

# FileNotFoundError

try:
    with open('hello.txt', 'w') as f:
        pass
    raise BadUserError()
except FileNotFoundError:
    print('Sorry :( File does not exists!')
except BadUserError:
    print('Bad user')
finally:
    print('Finally has been executed')
print("Program didn't stop")
