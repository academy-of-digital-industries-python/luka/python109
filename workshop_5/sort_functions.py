from typing import Any


def beautiful_print(value: Any) -> None:
    print('-' * 10)
    print(value)
    print('-' * 10)


def sort_the_list(numbers: list[int]) -> list[int]:
    nums2 = []

    for number in numbers:
        if not nums2:
            nums2.append(number)
            continue

        for i, num2 in enumerate(nums2):
            if number > num2:
                if len(nums2) - 1 == i:
                    nums2.append(number)
                    break
                continue
            else:
                nums2.insert(i, number)
                break
    return nums2
