class Dog:
    def __init__(self, name: str, age: int = 0) -> None:
        """
        initializer / constructor
        dunder method / magic methods __method_name__
        """
        # attributes
        print(self)
        self.name = name
        self.age = age
    
    def sit(self) -> None:
        print(f'{self.name} is sitting')

    def roll_over(self) -> None:
        print(f'{self.name} is rolling over')

# instance / object
dog_1: Dog = Dog("Larry")  # instantiation / object creation
print(f'dog 1', dog_1)
dog_2: Dog = Dog("Dog 2", 12)
print(dog_2)

print(dog_1.name)
print(dog_1.age)
dog_1.sit()

print(dog_2.name)
print(dog_2.age)
dog_2.roll_over()