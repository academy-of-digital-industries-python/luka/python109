treasure_map = [
    [1, 1, 1, 1],
    [1, 0, 0, 1],
    [1, 0, 2, 1],
    [1, 1, 1, 1]
]

print(treasure_map)

for row in treasure_map:
    "Nested For loop"
    for cell in row:
        if cell == 1:
            print('X', end='')
        elif cell == 2:
            print('C', end='')
        else:
            print('.', end='')
    print()
    
