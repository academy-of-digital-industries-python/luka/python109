from typing import Any

def print_list(_list: list[int]) -> None:
    for i, number in enumerate(_list):
        print(f'[{i}] {number}')
    print()


numbers = [1, 2, 3, 4, 5]
numbers2 = [-1, -2, -3, -4, -5]

print_list(numbers)
print_list(numbers2)


def add(value: Any, _list: list[Any] = None) -> Any:
    if _list is None:
        _list = []

    _list.append(value)
    print(_list)


add(5, numbers)  # [5]
add(6, numbers[:])  # [6]
add(7)  # [7]
add(8)  # [8]

print(numbers)

