# print("Hello World")
# print(5 + "5")

# ეს კოდი ამატებს ორ რიცხვს!
print(5 + 4)

print("5" + "5")

# declaration
result = 5 + 4 + 9 + 10   # evaluation - შეფასება
name = "John"
# print(result)
# (=) assignment operator
# print(name)

print(result + 17)
