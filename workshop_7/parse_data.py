from pprint import pprint

with open('football_games.csv') as f:
    columns: list[str] = f.readline().strip().split(',')
    games: list[dict] = []
    for line in f.readlines():
        values = line.strip().split(',')
        games.append(dict(zip(columns, values)))

    pprint(games)