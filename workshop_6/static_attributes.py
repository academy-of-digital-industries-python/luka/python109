from typing import ClassVar

class A:
    # static attribute
    __count: ClassVar[int] = 0  # private

    def __init__(self, name: str) -> None:
        # attribute
        self.name = name  # public 
        A.__count += 1

    @staticmethod  # decorator / wrapper
    def give_me_count() -> int:
        # getter
        return A.__count

a1 = A('a1')
# print(A.__count)
a2 = A('a2')

print(a1.name)
# print(a1.give_me_count())
print(a2.name)
# print(a2.give_me_count())
print(A.give_me_count())


