# for i in range(10, 0, 1):
#     print("hello")

is_raining = True
i = 0
while is_raining:
    i += 1
    print(f'I\'m sad {i}')
    if i >= 50:
        is_raining = False

i = 0 
while True:
    i += 1
    if i % 2 == 1:
        # print('odd')
        continue

    print(f'I\'m sad {i}')
    
    if i >= 50:
        break