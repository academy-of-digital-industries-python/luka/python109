import random
import datetime


team_names = [
    'Arsenal',
    'Aston Villa',
    'Bournemouth',
    'Brentford',
    'Brighton',
    'Burnley',
    'Chelsea',
    'Crystal Palace',
    'Everton',
    'Fulham',
    'Leeds United',
    'Leicester City',
    'Liverpool',
    'Man. City',
    'Manchester Utd',
    'Newcastle',
    'Norwich City',
    'Nottingham',
    'Southampton',
    'Tottenham',
    'Watford',
    'West Ham',
    'Wolves',
]

with open('football_games.csv', 'w') as f:
    """
    Create Match/Game class
    home_team != away_team
    """
    games: list[dict] = [
        {
            'league': 'Premier League',
            'start_date': datetime.date(year=random.randint(2022, 2023), 
                                        month=random.randint(1, 12),
                                        day=random.randint(1, 28)),
            'home_team': random.choice(team_names),
            'away_team': random.choice(team_names),
            'home_score': random.randint(1, 7),
            'away_score': random.randint(1, 7),
        } for _ in range(380)
    ]
    columns: list[str] = ['league', 'start_date', 'home_team', 'away_team', 'home_score', 'away_score']
    f.write(','.join(columns))
    f.write('\n')
    
    for game in games:
        f.write(','.join([str(value) for value in game.values()]))
        f.write('\n')
        
    print(games)
