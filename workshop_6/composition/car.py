from brand import Brand

class Car:
    def __init__(self, 
                 brand: Brand, 
                 model: str, 
                 assembly_year: int) -> None:
        self.brand = brand  # Composition
        self.model = model
        self.assembly_year = assembly_year
    
    def get_descriptive_name(self) -> str:
        return f'{self.brand.get_descriptive_name()} - {self.model}'

mercedes: Brand = Brand('Mercedes', 1926)
tesla: Brand = Brand('Tesla', 2003)

my_car: Car = Car(mercedes, 'EQB', 2023)
my_car_2: Car = Car(mercedes, 'EQE', 2023)
my_car_3: Car = Car(tesla, 'Model S', 2020)

# print(my_car.brand, my_car.model, my_car.assembly_year)
print(my_car.get_descriptive_name())
print(my_car_2.get_descriptive_name())
print(my_car_3.get_descriptive_name())
