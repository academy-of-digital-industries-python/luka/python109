names: tuple[str] = tuple(f'Name {i}' for i in range(3))

print(names)
# unpacking

# name1, name2, name3 = 'name 1', 'name 2', 'name 3'
name1, name2, name3 = names


# a = (1, 2, 3, 4)
# print(a)
print(name1, name2, name3)