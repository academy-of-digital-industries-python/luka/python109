""" working with files """
import time
# read (r)
"""
f = open('./workshop_7/data.txt')
print(f.read())
f.close()
"""

# context manager
with open('./workshop_7/data.txt', 'r') as f:
    # print(f.read())
    # print(f.readlines())
    print(f.tell())
    users = [
        user.strip()
        for user in f.readlines()
    ]
    print(f.tell())  # EOF - end of file
    # f.seek(22)
    # print(f.tell())
    print(users)
    # print(f.read())

users[5] = 'mari7'
users.append('John')
# write (w)
"""
with open('./workshop_7/data.txt', 'w') as f:
    for user in users:
        f.write(f'{user}\n')

    f.writelines([
        f'{user}\n'
        for user in users
    ])
"""

# append (a)
with open('./workshop_7/data.txt', 'a') as f:
    print(users)
    f.writelines([
        f'{user}\n'
        for user in users[-3:]
    ])

""" file paths """
# pathlib -> Path
# relative path
data_file = './workshop_7/data.txt'
print(data_file)

# absolute path
data_file = 'C:\\Users\\tavkh\\Workspace\\python109\\workshop_7\\data.txt'

print(data_file)