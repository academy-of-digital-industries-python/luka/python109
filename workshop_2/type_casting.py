"""
Action of switching data type
int -> str
bool -> str
float -> int
str -> bool
int -> bool
"""
number = 5
# print(type(number), str(number))
# print(True, str(True))
number = str(number)
print(type(number), number * 5)

number = int(float("5.6")) # 5
print(number)
# print(round(5.6, 0))

"""
what happens when we convert to bool?
Truthy and Falsy values

0, 1
0(10) = 0(2)
1(10) = 1(2)
2(10) = 10(2)
"""
print('Converting number to Boolean')
print(bool(1.1), bool(-1), bool(100), bool(0))

print('Converting str to Boolean\n')
print(bool(""))  # Falsy
print(bool(" "))
print(bool("Josh"))
print(bool("X"))
print(bool("x"))


