"""
Global Scope / გლობალური ხილვადობის არეალი
"""
age = 15
# Function definiton / ფუნქციის განსაზღვრა
def miesalme(name: str):
    """
    Function body / ფუნქციის ტანი

    Local Scope / ლოკალური ხილვადობის არეალი
    """
    print(f'გამარჯობა {name}, {age}')


if __name__ == '__main__':
    # Function call / ფუნქციის გამოძახება
    miesalme('Luka')
    miesalme('Nika')
    miesalme('Lela')
    miesalme('Nata')
    miesalme('Sandro')
