def greet(name: str) -> None:
    # function definition
    print(f'Hello {name}')
    print('how can I help you?')


def shekribe(a: float, b: float) -> float:
    print(f'shekribe was called with arguments: {a = }, {b = }')
    return a + b

def send_to_server(result: float) -> None:
    print(f'send_to_server was called with arguments: {result = }')
    print(result)

'''
result = greet("Luka")
print(result)
print(greet('Customer 1'))  # Function call
greet('Customer 2')  # Function call
greet('Customer 1')  # Function call
'''

num1 = 5
num2 = 6

send_to_server(
    shekribe(
        shekribe(num1, num2), shekribe(7, 8.9)
    )
)
