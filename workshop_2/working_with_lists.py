"""
Data Structure (მონაცემთა სტრუქტურა)
List (array, მასივი)
"""

name = 'George'  # string is a finite sequence of characters (სასრული მიმდევრობა characters)


"""
values in list (any data structure) is called element

in list each element has its own index, which starts from 0
"""

numbers = [12, 56, -45, 100, -15, 0, 17, 88.9]
names = ['Josh', 'Jane', 'Joe', 'Harry']

print(numbers)
# print(names)

# Access (წვდომა)
print(numbers[0])
print(numbers[2])
print(numbers[7])

# Update (განახლება)
numbers[3] = numbers[0] + numbers[1]
# Add (ჩამატება)
numbers.append(28)

# Remove (წაშლა)
numbers.pop(5)
numbers.remove(88.9)
# numbers.remove(88.9)

# numbers[8] = 28
print(numbers)

"""
ავიყვანოთ თითოეული ელემნტი კვადრატში
"""
for number in numbers:
    """
    თითოეულ შესვლას ციკლი ეწოდება იტერაცია/iteration (ბიჯი)
    """
    # code fragment
    print(number ** 2)
