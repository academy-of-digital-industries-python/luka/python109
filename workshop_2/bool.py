"""
Data type (მონაცემთა ტიპი):
Boolean 
bool - in python
only 2 value
    - True (ჭეშმარიტი)
    - False (მცდარი)
"""
is_windy = True
is_raining = False

print(f'is windy: {is_windy}', f'is raining: {is_raining}')
print(f'{is_windy = } {is_raining = }')


"""
Boolean operators
* and   - &&
* or    - ||
* not   - !
"""
print(is_windy and is_raining)  # True and False -> False

print('Truth table (and)')
print(f'{True and True = }')    # -> True
print(f'{True and False = }')   # -> False
print(f'{False and True = }')   # -> False
print(f'{False and False = }')  # -> False

went_cinema = True
ate_popcorn = False
print('Cinema Example (and)')
print(f'{went_cinema and not ate_popcorn = }')

print('Truth table (or)')
print(f'{True or True = }')    # -> True
print(f'{True or False = }')   # -> True
print(f'{False or True = }')   # -> True
print(f'{False or False = }')  # -> False

print('Cinema Example (or/not)')
print(f'{went_cinema or not ate_popcorn = }')

"""
1. what is not False? True
2. True or True? True 
"""
print(went_cinema)

print('-' * 10)
is_windy = True
is_raining = False
is_sunny = True

print(f'{is_windy and is_raining and is_sunny = }')

