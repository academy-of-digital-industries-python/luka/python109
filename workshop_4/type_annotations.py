'''
type hinting
'''
from typing import Any, List

name: str = 'Luka'
age: int = 17

friends: List[str] = [
    'George', 'Jena',' Erekle'
]

numbers: list[float] = [
    1.1, 1.2, 1.3, 1.99999, 2.1, 0, -1
]
even_numbers: tuple[int] = (2, 4, 6, 8)

student: dict[str, Any] = {
    'name': 'Luka',
    'age': 17
}

print(age)
