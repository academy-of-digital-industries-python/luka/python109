is_raining = True
is_cold = False

if is_raining and is_cold:  # თუ პირობა ჭეშმარიტია
    "if/elif/else block"
    # code fragment
    take_umbrella = True
    take_coat = True
elif is_raining and not is_cold:  # და თუ
    take_umbrella = True
    take_coat = False
elif not is_raining and is_cold:
    take_umbrella = False
    take_coat = True
else:  # თუ არადა
    take_umbrella = False
    take_coat = False

take_umbrella_2 = is_raining
take_coat_2 = is_cold
wear_t_shirt = not is_cold

print(f'{take_umbrella = }, {take_coat = }')
print(f'{take_umbrella_2 = }, {take_coat_2 = }')

"""
If without else

>
>=
<
<=
!=
"""
name = 'Mary'
age = 60
welcome_text = f'Hello {name}, I am {age} years old'
if 18 <= age < 50:
    welcome_text += ' I am an Adult'
elif age >= 50:
    welcome_text += ' I am a Senior'

# else:
#     welcome_text += ' I am a Minor'

print(welcome_text)
