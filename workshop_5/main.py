# from sort_functions import beautiful_print
# import sort_functions
import sort_functions as sf
from sort_functions import beautiful_print as bp, sort_the_list
import random

# namespace 
# from sort_functions import *

def beautiful_print():
    pass


def main() -> None:
    '''
    sf.beautiful_print('Hello')
    # beautiful_print('Hello *')
    # beautiful_print('Hello')

    bp('Hello bp')
    '''

    nums = [random.randint(-100, 100) for _ in range(10)]
    print(nums)
    sorted_nums = sort_the_list(nums)
    print(sorted_nums)
    nums.sort()



if __name__ == '__main__':
    """
    main guard clause
    """
    main()
